[%%server
  open Eliom_content.Html

  let include_js file =
    (* Some files that we serve are static and stored in the "static" directory
     * of the ocsigen server. Below we simply define a helper function to build
     * URIs for files in that directory. *)
    let static_uri = F.make_uri ~service:(Eliom_service.static_dir ()) in
    F.(script ~a:[a_src (static_uri ["js"; file ])] (pcdata ""))

  let include_quill = include_js "quill.min.js"
  let include_highlight = include_js "highlight.min.js"
]

[%%client
  (* We need to create a JS Quill object. When creating it, we can pass
   * parameters through JSON (roughly). Which we'll do as a JS script that we'll
   * inject in the page. Note that the code will be parsed and checked at
   * compile-time. *)
  let setup ?(writable = true) ?toolbar_id editor =
    let open Js in
    let options = object%js
      val modules = object%js
        val toolbar =
          match toolbar_id with
          | Some id -> some (string ("#" ^ id))
          | None -> null
        val syntax = _false
      end
      val placeholder = string "Write here"
      val readOnly = not writable
      val theme = string "snow"
    end
    in

    (* The new%js syntax is a PPX which allows us to easily create JS objects as
     * if we used "new" directly in JS. *)
   Prose_formats.Quill.Quill.t editor options
]

module Toolbar = struct
  (* Colors available in the color pickers. Directly copied from the ones used
   * by Quill. They will be formatted as hex colors. *)
  let colors = [
    0, 0, 0;
    230, 0, 0;
    255, 153, 0;
    255, 255, 0;
    0, 138, 0;
    0, 102, 204;
    153, 51, 255;
    250, 204, 204;
    255, 235, 204;
    255, 255, 204;
    204, 232, 204;
    204, 224, 245;
    235, 214, 255;
    187, 187, 187;
    240, 102, 102;
    255, 194, 102;
    255, 255, 102;
    102, 185, 102;
    102, 163, 224;
    194, 133, 255;
    136, 136, 136;
    161, 0, 0;
    178, 107, 0;
    178, 178, 0;
    0, 97, 0;
    0, 71, 178;
    107, 36, 178;
    68, 68, 68;
    92, 0, 0;
    102, 61, 0;
    102, 102, 0;
    0, 55, 0;
    0, 41, 102;
    61, 20, 102;
  ]
  |> List.map (fun (r, g, b) -> Printf.sprintf "#%06x" (b + 255 * (g + 255 * r)))

  (* Hierarchical list of options that we want in the toolbar.
   * Every button will be in a single <span> in order to save some space. *)
  let options =
    (* Helper to create values that hold the option name, its values, and the
     * optional prefix that will be prepended to the values in the UI. *)
    let mk ?prefix ?label elt values =
      elt, values, prefix, label
      in
    [
      [
        mk "bold" [];
        mk "italic" [];
        mk "underline" [];
        mk "strike" [];
        mk "link" [];
        mk ~prefix:"Color " "color" colors;
        mk ~prefix:"Color " "background" colors;
        mk "blockquote" [];
        mk "code" [];
        (* Code blocks are disabled for now: they don't seem to work that well
         * and the syntax highlighting pulls a lot of JS code (it pretty much
         * adds 50% to the page load size).
         * mk "code-block" []; *)
        mk ~label:"Indent" "indent" [ "-1" ];
        mk ~label:"Unindent" "indent" [ "+1" ];
        mk ~label:"Ordered list" "list" [ "ordered" ];
        mk ~label:"Unordered list" "list" [ "bullet" ];
        mk ~label:"Checklist" "list" [ "check" ];
        (* NOTE: default alignment for Quill is "", not "left". *)
        mk "align" [ ""; "center"; "right"; "justify" ];
        mk ~label:"Subscript" "script" [ "sub" ];
        mk ~label:"Superscript" "script" [ "super" ];
        mk "image" [];
        mk "video" [];
        mk ~prefix:"Heading " "header" [ "1"; "2"; "3"; "4"; "5"; "" ];
      ];
    ]

  (* Create a subtree that Quill can reuse to put its toolbar buttons. It will
   * look for elements of class ql-${foo}, extract the button descriptions
   * stored in them and will create its shiny toolbar buttons. *)
  let toolbar ~toolbar_id options =
    let open D in
    ListLabels.map options ~f:(fun sub ->
      ListLabels.map sub ~f:(fun (name, values, prefix, label) ->
        (* Quill will look for items with class ql-${foo} where ${foo} is the
         * edition operation: bold, italic, size, font, ... *)
        let cls = a_class [ "ql-" ^ name ] in
        (* Prepend the prefix if any was given. It makes it possible to turn
         * values such as heading from merely "1" to "Heading 1". *)
        let prefix s = match prefix with Some p when s <> "" -> p^s | _ -> s in
        (* Set of classes shared by toplevel items created below, no matter
         * their type. *)
        let default_classes = [cls; a_title (String.capitalize_ascii name)] in
        (* Helper to create a button. *)
        let button name supplemental_classes =
          button ~a:(supplemental_classes @ default_classes) [ pcdata @@
            match label with
            | Some label -> label
            | None -> prefix name
          ]
        in
        (* Options are handled differently depending on the number of values
         * they can take.
         * Options with 0 value are on/off buttons.
         * Options with 1 value are buttons that apply the corresponding
         * change and can be applied several times (think +1 indent buttons).
         * Options with more values are drop-down lists with the first element
         * selected by default. *)
        match values with
        | [] ->
            button name []
        | [ single ] ->
            (* NOTE: "value" isn't a value meaningful to <button> elements but
             * Quill it is what Quill expects. *)
            button name [ Unsafe.string_attrib "value" single ]
        | _ ->
            ListLabels.map values ~f:(fun v ->
              (* Quill's way of saying "default" is to use "" so select that by
               * default and we have to put no value for that case, at least as
               * far as "heading" is concerned (it seems other selects are
               * different). *)
              option
                ~a:[ if v = "" then a_selected () else a_value v ]
                (pcdata (prefix v))
            )
            |> select ~a:default_classes
      )
      |>
      span ~a:[ a_class [ "ql-formats" ] ]
    )
    |> div ~a:[ a_id toolbar_id ]

end
